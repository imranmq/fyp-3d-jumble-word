﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.UI;
public class ScoreBoardLogic : MonoBehaviour {


    public GameObject NameUserPrefab;
    public GameObject ScoreUserPrefab;
    public GameObject RowLinePrefab;    
    private List<KeyValuePair<string, string>> MyListScore = new List<KeyValuePair<string, string>>();
	// Use this for initialization
	void Start () {





        var rlist = ReadFromFile();
        
        int numToInIt = rlist.Count;
        var i = 0;
        foreach (var l in rlist)

        {

            if( i < 4)
            {
                GameObject ParentCanvas = GameObject.FindGameObjectWithTag("ScoreBoardCanvas");
                GameObject ScoreParentCavas = GameObject.FindGameObjectWithTag("ScoreCanvas");
                if (i == 0)
                {

                    Vector3 myVectorName = new Vector3(-150, 50, 0);
                    Vector3 myVectorRow = new Vector3(10, 30, 0);
                    Vector3 myVectorScore = new Vector3(150, 50, 0);
                    GameObject NamePrefabGen = Instantiate(NameUserPrefab, myVectorName, Quaternion.identity) as GameObject;
                    GameObject RowPrefabGen = Instantiate(RowLinePrefab, myVectorRow, Quaternion.identity) as GameObject;
                    GameObject ScorePrefabGen = Instantiate(ScoreUserPrefab, myVectorScore, Quaternion.identity) as GameObject;




                    NamePrefabGen.transform.SetParent(ParentCanvas.transform, false);
                    RowPrefabGen.transform.SetParent(ParentCanvas.transform, false);
                    ScorePrefabGen.transform.SetParent(ParentCanvas.transform, false);

                    NamePrefabGen.transform.Translate(myVectorName);
                    RowPrefabGen.transform.Translate(myVectorRow);
                    ScorePrefabGen.transform.Translate(myVectorScore);

                    NamePrefabGen.GetComponent<Text>().text = l.Key;
                    ScorePrefabGen.GetComponent<Text>().text = l.Value;



                }
                else
                {
                    Vector3 myVectorName = new Vector3(-150, (50 - (60 * i)), 0);
                    Vector3 myVectorRow = new Vector3(10, 30 - (60 * i), 0);
                    Vector3 myVectorScore = new Vector3(150, 50 - (60 * i), 0);
                    GameObject NamePrefabGen = Instantiate(NameUserPrefab, myVectorName, Quaternion.identity) as GameObject;
                    GameObject RowPrefabGen = Instantiate(RowLinePrefab, myVectorRow, Quaternion.identity) as GameObject;
                    GameObject ScorePrefabGen = Instantiate(ScoreUserPrefab, myVectorScore, Quaternion.identity) as GameObject;


                    NamePrefabGen.transform.SetParent(ParentCanvas.transform, false);
                    RowPrefabGen.transform.SetParent(ParentCanvas.transform, false);
                    ScorePrefabGen.transform.SetParent(ParentCanvas.transform, false);

                    NamePrefabGen.transform.Translate(myVectorName);
                    RowPrefabGen.transform.Translate(myVectorRow);
                    ScorePrefabGen.transform.Translate(myVectorScore);

                    NamePrefabGen.GetComponent<Text>().text = l.Key;
                    ScorePrefabGen.GetComponent<Text>().text = l.Value;
                }
            }
            i++;
        }
        
        
    }

    private List<KeyValuePair<string, string>> ReadFromFile()

    {

        var lines = File.ReadAllLines(Application.persistentDataPath + "/Test4.dat").Select(x => x.Split('-')).FirstOrDefault();

        var list = new List<KeyValuePair<string, string>>();



        foreach (string kvp in lines)

        {

            if (!string.IsNullOrEmpty(kvp.Split(',').FirstOrDefault()))

                list.Add(new KeyValuePair<string, string>(kvp.Split(',').FirstOrDefault(), kvp.Split(',').LastOrDefault()));

        }
        return list;

    }

    public void BackToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
