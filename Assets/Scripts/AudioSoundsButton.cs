﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Button))]
public class AudioSoundsButton : MonoBehaviour {

    public AudioClip ButtonClickSound;
    private Button button { get { return GetComponent<Button>(); } }
    private AudioSource source { get { return GetComponent<AudioSource>(); } }
	// Use this for initialization
	void Start () {
        gameObject.AddComponent<AudioSource>();
        source.clip = ButtonClickSound;
        source.playOnAwake = false;
        button.onClick.AddListener(() => PlaySound());        
	}
	
    void PlaySound()
    {
        source.PlayOneShot(ButtonClickSound);
    }
}
