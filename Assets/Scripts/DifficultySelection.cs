﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class DifficultySelection : MonoBehaviour {

    
    private GameManager gamemanagaer;

    private void Awake()
    {
        gamemanagaer = GetComponent<GameManager>();
    }
    public void StartEasyGame()
    {      
        gamemanagaer.setDifficulty("Easy");
        SceneManager.LoadScene("Game");       
    }


    public void StartMediumGame()
    {    
        gamemanagaer.setDifficulty("Medium");
        SceneManager.LoadScene("Game");      
    }

    public void StartHardGame()
    {     
        gamemanagaer.setDifficulty("Hard");
        SceneManager.LoadScene("Game");       
    }


}
