﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PausedInGameLogic : MonoBehaviour {

	public void ToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void Resume()
    {
        GameObject TimeLogicObj = GameObject.Find("MyTimerLogic");
        TimeLogicObj.GetComponent<TimerScript>().startTimer();
        this.gameObject.SetActive(false);
       
    } 

    public void PauseGame() {
        GameObject TimeLogicObj = GameObject.Find("MyTimerLogic");
        TimeLogicObj.GetComponent<TimerScript>().stopTimer();
        this.gameObject.SetActive(true);
       


    }
}
