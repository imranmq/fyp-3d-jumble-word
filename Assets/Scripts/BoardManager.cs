﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class BoardManager : MonoBehaviour
{
    private GameManager mainGameManager;

    private List<GameObject> SelectedAlphabets = new List<GameObject>();
    private List<GameObject> AllAlphabetsInitialized = new List<GameObject>();
    public GameObject alphabet;
    public GameObject SelectedTextObject;
    public GameObject UpperBoardRowPlaneEasy;
    public GameObject UpperBoardRowPlaneMedium;
    public GameObject HelveticaText;
    private GameObject selectedWord;

    private string selectedDiffiuclty = "";
    // Dictionary Word File Reader 
    protected FileInfo theSourceFile = null;
    protected StreamReader reader = null;
    private string[] Alphabet = new string[31] { "A", "A", "B", "C", "D", "E", "E", "F", "G", "H", "I", "I", "J", "K", "L", "M", "N", "O", "O", "P", "Q", "R", "S", "T", "U", "U", "V", "W", "X", "Y", "Z" };
    private Hashtable myHastableOfWords = new Hashtable();
    private int loadCount = 178691;    
    public string EndScore = "0";
    // Use this for initialization
    void Start()
    {

        FileInfo theSourceFile = new FileInfo("Assets/Dictionary/words.txt");
        StreamReader reader = theSourceFile.OpenText();
        do
        {
            string text = reader.ReadLine();
            if (text != null)
            {
                myHastableOfWords.Add(text, text);
                loadCount--;
            }
        } while (loadCount > 0);

      
        mainGameManager = GetComponent<GameManager>();
   
    }

   
    public void SetUpBoard(string difficulty)
    {

        if (difficulty == "Easy")
        {
            selectedDiffiuclty = "Easy";
            setEasyDifficultyUpperBoard();
        }
        if (difficulty == "Medium")
        {
            selectedDiffiuclty = "Medium";
            setMediumDifficultyUpperBoard();
        }
        if (difficulty == "Hard")
        {
            selectedDiffiuclty = "Hard";
            setHardDifficultyUpperBoard();
        }
    }

   
    void Timer(int time){

        GameObject TimeLogicObject = GameObject.Find("MyTimerLogic");
        TimeLogicObject.GetComponent<TimerScript>().setTime(time , false);
    }

    public void stopTimer()
    {
        GameObject TimeLogicObject = GameObject.Find("MyTimerLogic");
        TimeLogicObject.GetComponent<TimerScript>().stopTimer();
    }

    void AddTime()
    {
        int time = 0;
        if(selectedDiffiuclty == "Easy")
        {
            time = 120;
        }
        else if (selectedDiffiuclty == "Medium")
        {
            time = 60;
        }
        else if (selectedDiffiuclty == "Hard")
        {
            time = 30;
        }
        GameObject TimeLogicObject = GameObject.Find("MyTimerLogic");
        TimeLogicObject.GetComponent<TimerScript>().setTime(time, true);
    }
   
    void setEasyDifficultyUpperBoard()
    {
       
        selectedWord = Instantiate(SelectedTextObject, new Vector3(265, 905, 1639), Quaternion.identity) as GameObject;
        selectedWord.name = "SelectedLetters";
        selectedWord.GetComponent<SimpleHelvetica>().Text = "";
        selectedWord.GetComponent<SimpleHelvetica>().GenerateText();
        GameObject planeinstance1 = Instantiate(UpperBoardRowPlaneEasy, new Vector3(27, 7, 20), Quaternion.identity) as GameObject;
        GameObject planeinstance2 = Instantiate(UpperBoardRowPlaneEasy, new Vector3(27, -3, 20), Quaternion.identity) as GameObject;

      
        if (alphabet == null)
        {
            Debug.Log("IT'S NULL!!!");
        }
        else
        {

            for (int i = 0; i < 4; i++)
            {
                GameObject alpha1 = Instantiate(alphabet, new Vector3((i * 14) + 4, 10, 20), Quaternion.identity) as GameObject;
                GameObject alpha2 = Instantiate(alphabet, new Vector3((i * 14) + 4, -1, 20), Quaternion.identity) as GameObject;

                alpha1.GetComponent<TextMesh>().text = Alphabet[Random.Range(0, Alphabet.Length)];
                alpha2.GetComponent<TextMesh>().text = Alphabet[Random.Range(0, Alphabet.Length)];
                AllAlphabetsInitialized.Add(alpha1);
                AllAlphabetsInitialized.Add(alpha2);

            }

        }
        Timer(120);
    }

    void setMediumDifficultyUpperBoard()
    {
        
        selectedWord = Instantiate(SelectedTextObject, new Vector3(265, 905, 1639), Quaternion.identity) as GameObject;
        selectedWord.name = "SelectedLetters";
        selectedWord.GetComponent<SimpleHelvetica>().Text = "";
        selectedWord.GetComponent<SimpleHelvetica>().GenerateText();

        GameObject planeinstance1 = Instantiate(UpperBoardRowPlaneMedium, new Vector3(27, 10, 20), Quaternion.identity) as GameObject;
        GameObject planeinstance2 = Instantiate(UpperBoardRowPlaneMedium, new Vector3(27, 2, 20), Quaternion.identity) as GameObject;
        GameObject planeinstance3 = Instantiate(UpperBoardRowPlaneMedium, new Vector3(27, -6, 20), Quaternion.identity) as GameObject;

    
        if (alphabet == null)
        {
            Debug.Log("IT'S NULL!!!");
        }
        else
        {

            for (int i = 0; i < 8; i++)
            {
                GameObject alpha1 = Instantiate(alphabet, new Vector3((i * 7) + 1, 11.7f, 20), Quaternion.identity) as GameObject;
                alpha1.GetComponent<TextMesh>().fontSize = 22;
                GameObject alpha2 = Instantiate(alphabet, new Vector3((i * 7) + 1, 3.5f, 20), Quaternion.identity) as GameObject;
                alpha2.GetComponent<TextMesh>().fontSize = 22;
                GameObject alpha3 = Instantiate(alphabet, new Vector3((i * 7) + 1, -5, 20), Quaternion.identity) as GameObject;
                alpha3.GetComponent<TextMesh>().fontSize = 22;

                alpha1.GetComponent<TextMesh>().text = Alphabet[Random.Range(0, Alphabet.Length)];
                alpha2.GetComponent<TextMesh>().text = Alphabet[Random.Range(0, Alphabet.Length)];
                alpha3.GetComponent<TextMesh>().text = Alphabet[Random.Range(0, Alphabet.Length)];

                AllAlphabetsInitialized.Add(alpha1);
                AllAlphabetsInitialized.Add(alpha2);
                AllAlphabetsInitialized.Add(alpha3);
            }

        }
        Timer(60);
    }

    void setHardDifficultyUpperBoard()
    {
      
        selectedWord = Instantiate(SelectedTextObject, new Vector3(265, 905, 1639), Quaternion.identity) as GameObject;
        selectedWord.name = "SelectedLetters";
        selectedWord.GetComponent<SimpleHelvetica>().Text = "";
        selectedWord.GetComponent<SimpleHelvetica>().GenerateText();

        GameObject planeinstance1 = Instantiate(UpperBoardRowPlaneMedium, new Vector3(27, 10, 20), Quaternion.identity) as GameObject;
        GameObject planeinstance2 = Instantiate(UpperBoardRowPlaneMedium, new Vector3(27, 2, 20), Quaternion.identity) as GameObject;
        GameObject planeinstance3 = Instantiate(UpperBoardRowPlaneMedium, new Vector3(27, -6, 20), Quaternion.identity) as GameObject;

     
        if (alphabet == null)
        {
            Debug.Log("IT'S NULL!!!");
        }
        else
        {

            for (int i = 0; i < 12; i++)
            {
                GameObject alpha1 = Instantiate(alphabet, new Vector3((i * 4.5f) + 1, 11.7f, 20), Quaternion.identity) as GameObject;
                alpha1.GetComponent<TextMesh>().fontSize = 20;
                GameObject alpha2 = Instantiate(alphabet, new Vector3((i * 4.5f) + 1, 3.5f, 20), Quaternion.identity) as GameObject;
                alpha2.GetComponent<TextMesh>().fontSize = 20;
                GameObject alpha3 = Instantiate(alphabet, new Vector3((i * 4.5f) + 1, -5, 20), Quaternion.identity) as GameObject;
                alpha3.GetComponent<TextMesh>().fontSize = 20;

                alpha1.GetComponent<TextMesh>().text = Alphabet[Random.Range(0, Alphabet.Length)];
                alpha2.GetComponent<TextMesh>().text = Alphabet[Random.Range(0, Alphabet.Length)];
                alpha3.GetComponent<TextMesh>().text = Alphabet[Random.Range(0, Alphabet.Length)];


                AllAlphabetsInitialized.Add(alpha1);
                AllAlphabetsInitialized.Add(alpha2);
                AllAlphabetsInitialized.Add(alpha3);
            }

        }
        Timer(30);
    }

    public void selectedLettersList(GameObject clickedGameObject)
    {
        SelectedAlphabets.Add(clickedGameObject);
        //Debug.Log(this.SelectedAlphabets.Count);

    }

    public void bringBackLastLetterFromSelectedLetterList()
    {
     
        

        if (SelectedAlphabets.Count > 0)
        {
            GameObject lastItem = SelectedAlphabets[SelectedAlphabets.Count - 1];
            lastItem.SetActive(true);
            SelectedAlphabets.RemoveAt(SelectedAlphabets.Count - 1);
        }
    } 

    public void CheckWord()
    {
      
     //   Debug.Log("Check Word");
        string currentWord = selectedWord.GetComponent<SimpleHelvetica>().Text;
      //  Debug.Log(myHastableOfWords.Count);
        bool matchFound = myHastableOfWords.ContainsValue(currentWord);       
       // Debug.Log(matchFound);
        if (matchFound)
        {
            generateNewRandomLetters(SelectedAlphabets);
            AddTime();
            GameObject CalculateScoreObject = GameObject.Find("MyScoreLogic");
            CalculateScoreObject.GetComponent<ScoreLogic>().CalculateScore(currentWord);
            string found = CheckPossibleCombinations();
            if (found == string.Empty)
            {
                generateNewRandomLetters(AllAlphabetsInitialized);
            }
            else
            {
                //Debug.Log("Found: " + found);
            }
        }    
    }

    public string CheckPossibleCombinations()
    {
        var tempString = "";
        AllAlphabetsInitialized.ForEach(delegate (GameObject item)
        {
            string currentLetter = item.GetComponent<TextMesh>().text;
            tempString += currentLetter;
        });

        var q = tempString.Select(x => x.ToString());
        int size = tempString.Length;

        for (int i = 0; i < size - 1; i++)
        {
            q = q.SelectMany(x => tempString, (x, y) => x + y);
            if (i > 0)
            {
                foreach (var item in q)
                {
                    if (myHastableOfWords.ContainsValue(item))
                    {
                        return item;
                    }

                }
            }
        }

        return string.Empty;

    }




    public void generateNewRandomLetters(List<GameObject> LettersObjects)
    {
        LettersObjects.ForEach(delegate (GameObject item)
        {
            if (!item.activeSelf)
            {
                item.SetActive(true);
            }            
            selectedWord.GetComponent<SimpleHelvetica>().Text = "";
            selectedWord.GetComponent<SimpleHelvetica>().GenerateText();
            selectedWord.GetComponent<SelectedLetters>().setBoardString("", false);
            item.GetComponent<TextMesh>().text = Alphabet[Random.Range(0, Alphabet.Length)];


        });
    }      
    
    public void ClearStoredListOnGameEnd()
    {
        SelectedAlphabets.Clear();
        AllAlphabetsInitialized.Clear();

    }
}
