﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Timers;
using UnityEngine.UI;
public class TimerScript : MonoBehaviour
{
    public Text TimerText;
    private Timer myTimer = null;

    private GameManager GameManagerEndCall;
    private GameObject GameManagerObject;

    
    private bool TimerRunning = false;    
    //public int time = 0;
    private float time;
    public void startTimer()
    {

 	
		TimerText.text="";
        myTimer = new Timer();
        myTimer.Elapsed += new ElapsedEventHandler(OnTimedEventCall);
        myTimer.Interval = 1000;
        myTimer.Enabled = true;

         string convertedTime = time.ToString();
	
        

    }
    public void resumeTimer()
    {
        myTimer.Enabled = true;
    }
        

 void Start()
 {
        GameManagerObject = GameObject.Find("GameManager");
        GameManagerEndCall = GameManagerObject.GetComponent<GameManager>();

  }

 void Update()
 {
        TimerText.text = time.ToString();
 }


    private void OnTimedEventCall(object source, ElapsedEventArgs e)
    {
        TimeCountDown();		
		
    }

    void TimeCountDown()
    {

        if (time < 1)
        {                           
            myTimer.Enabled = false;
            GameManagerEndCall.endCurrentGame();
        }
        else
        {
            time = time - 1;                    
        }     
    }
    public void stopTimer()
    {
        myTimer.Enabled = false;
    }
    public void setTime(int time , bool addTime)
    {
      
        if (!addTime)
        {
      //      Debug.Log("SetTime Called");
            this.time = time;
            string convertedTime = this.time.ToString();

            startTimer();
        }
        if (addTime)
        {
       //     Debug.Log("prev Time : " +this.time);
        //    Debug.Log("addition Time: " + time);
            this.time = this.time + time;
        }
    }

}
