﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckWord : MonoBehaviour
{


    private GameObject mainGameManagerObject;
    private BoardManager mainGameManager;
    // Use this for initialization
    void Start()
    {
        mainGameManagerObject = GameObject.Find("GameManager");
        mainGameManager = mainGameManagerObject.GetComponent<BoardManager>();
    }

    public void CheckWordBoardManagerCall()
    {
        mainGameManager.CheckWord();

    }
}
	