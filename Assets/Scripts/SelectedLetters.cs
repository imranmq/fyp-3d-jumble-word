﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectedLetters : MonoBehaviour
{

    public static string BoardString = "";
    private AudioSource Audio;
    private void Start()
    {
        Audio = GetComponent<AudioSource>();
    }
    public void setBoardString(string letter, bool add)
    {       
    //    Debug.Log(add);
        if (add)
        {
       //     Debug.Log("String plus letter : " +BoardString);

            SelectedLetters.BoardString = SelectedLetters.BoardString + letter;
      //      Debug.Log(BoardString);
            updateView();
        }
        if (!add)
        {


            SelectedLetters.BoardString = letter;
      //      Debug.Log("Justletter : " + BoardString);
        }
      

    }
	void updateView(){
		gameObject.GetComponent<SimpleHelvetica>().Text = BoardString;
		gameObject.GetComponent<SimpleHelvetica>().GenerateText();
	}

	public void OnMouseDown(){
        Audio.Play();
        GameObject findSelectedLetters = GameObject.Find("SelectedLetters");
        GameObject Manager = GameObject.Find("GameManager");
        string tempBoard = findSelectedLetters.GetComponent<SimpleHelvetica>().Text;
        if (tempBoard.Length > 0)
        {
            int lastIndexToRemove = tempBoard.Length - 1;
            //Debug.Log(lastIndexToRemove);
            string RemovedLetter = tempBoard.Remove(lastIndexToRemove);
            //Debug.Log(BoardString);
            findSelectedLetters.GetComponent<SimpleHelvetica>().Text = RemovedLetter;
            findSelectedLetters.GetComponent<SimpleHelvetica>().GenerateText();
            setBoardString(RemovedLetter, false);

            Manager.GetComponent<BoardManager>().bringBackLastLetterFromSelectedLetterList();
        }

    }
    
}
