﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class ScoreLogic : MonoBehaviour {

    public Text scoreText;
    private string scoreValue;

    private string[] Alphabet = new string[26] { "A" , "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
    private int[] Points = new int[26] { 1, 4, 3, 2, 1, 5, 6, 8, 1, 7, 8, 6, 5, 5, 1, 4, 10, 7, 4, 4, 2, 9, 7, 10, 6, 9 };
    private Dictionary<string, int> AlphabetsPoints = new Dictionary<string, int>();


	// Use this for initialization
	void Start () {
        for ( int i = 0; i < 26; i++)
        {
            AlphabetsPoints.Add(Alphabet[i], Points[i]);
        }
        scoreValue = "0";
	}
	


	// Update is called once per frame
	void Update () {
        scoreText.text = scoreValue;
	}

    public void CalculateScore(string word)
    {
     

        char[] wordString = word.ToCharArray();

        int scoreFromWord = 0;
        int.TryParse(scoreValue, out scoreFromWord);
        //Debug.Log("PRevScore: " + scoreFromWord);
        for ( var i = 0; i < wordString.Length -1; i++)
        {
            string tempStringAlphabet = wordString[i].ToString();
            bool foundAlphabet = AlphabetsPoints.ContainsKey(tempStringAlphabet);
            if (foundAlphabet)
            {
                int tempValue = AlphabetsPoints[tempStringAlphabet];
                scoreFromWord = scoreFromWord + tempValue;
                //Debug.Log("tempValue" + tempValue);
                //Debug.Log("scoreFromWord" + scoreFromWord);
               
                

            }
            
        }
        scoreValue = scoreFromWord.ToString();
        GameObject boardManager = GameObject.Find("GameManager");
        boardManager.GetComponent<BoardManager>().EndScore = scoreValue;
    }
}
