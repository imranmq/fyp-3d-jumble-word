﻿
using UnityEngine;
using System;
using System.IO;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour {


    // Singleton Game Object 
    private static bool created = false;

    // Main Game
    public static bool GameStart = false;
    private BoardManager boardScript;
    public static string difficulty = "";
    private bool CurrentRoundGameEnd = false;
    // Scenes
    Scene m_Scene;

 
        
    protected string text = " "; // assigned to allow first line to be read below    
    protected int loadcount = 450000;
            
	// Use this for initialization
	void Start () {               
       // do
       // {
       //     text = reader.ReadLine();
       //     dictonary.Add(text,loadcount);
       // //    Console.WriteLine(text);
            
       //     loadcount--;
       // }  while (loadcount >450000);      

       // print("helo");    	
	}
	
	
    void Awake()
    {
        if (!created)
        {
            DontDestroyOnLoad(this.gameObject);
            created = true;
            boardScript = GetComponent<BoardManager>();
            
            //Debug.Log("Awake: " + this.gameObject);
        }
    }

  
    public void setDifficulty(string ChoosenDifficulty)
    {
        difficulty = ChoosenDifficulty;
        
    }
   
    public void StartGame()
    {
        m_Scene = SceneManager.GetActiveScene();
   //     Debug.Log(m_Scene.name);
        boardScript.SetUpBoard(difficulty);

    }

    // Update is called once per frame
    void Update()
    {
        m_Scene = SceneManager.GetActiveScene();
        if(m_Scene.name == "Game" && GameStart == false)
        {
            GameStart = true;
            StartGame();
        }
        if(m_Scene.name == "Game" && CurrentRoundGameEnd == true)
        {
            CurrentRoundGameEnd = false;
            SceneManager.LoadScene("EndGame");
        }
        if(m_Scene.name == "MainMenu")
        {
            created = false;
            GameStart = false;
            Destroy(gameObject, 1);
        }
    }

    public void endCurrentGame(){
        boardScript.ClearStoredListOnGameEnd();
        CurrentRoundGameEnd = true;
        SelectedLetters.BoardString = "";
    }    

}
