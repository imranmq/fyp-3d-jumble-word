﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Linq;

public class MyEndGameScore : MonoBehaviour {

    public Text EndGameScore;
    public InputField NameImputField;
    public string PlayerName;
    private string TotalScore;
    
    private bool ToMainMenuCheck = false;
    Scene m_Scene;

    // Use this for initialization
    void Start () {
        GameObject GameManager = GameObject.Find("GameManager");
        string endGameScoreValue = GameManager.GetComponent<BoardManager>().EndScore;
        TotalScore = endGameScoreValue;
        EndGameScore.text = endGameScoreValue;

    }

    public void InputNameValueChange()
    {        
        PlayerName = NameImputField.text;
    }

    public void postScore()
    {
        var rlist = ReadFromFile();

        rlist.Add(new KeyValuePair<string, string>(PlayerName, TotalScore));
        WriteToFile(rlist);            
    }

    private void WriteToFile(List<KeyValuePair<string, string>> list)

    {

        using (StreamWriter file = new StreamWriter(Application.persistentDataPath + "/PlayerData.dat"))

        {

            foreach (KeyValuePair<string, string> val in list)

            {

                file.Write(val.Key + "," + val.Value + "-");

            }

        }

    }

    

    private List<KeyValuePair<string, string>> ReadFromFile()

    {

        var lines = File.ReadAllLines(Application.persistentDataPath + "/PlayerData.dat").Select(x => x.Split('-')).FirstOrDefault();

        var list = new List<KeyValuePair<string, string>>();



        foreach (string kvp in lines)

        {

            if (!string.IsNullOrEmpty(kvp.Split(',').FirstOrDefault()))

                list.Add(new KeyValuePair<string, string>(kvp.Split(',').FirstOrDefault(), kvp.Split(',').LastOrDefault()));

        }
        return list;

    }





    public void ToMainMenu()
    {
        postScore();
        
        SceneManager.LoadScene("MainMenu");

    }

    public void RestartCurrentGame()
    {
        postScore();
        GameManager.GameStart = false;
        SceneManager.LoadScene("Game");
    }    
}



[Serializable]
class PlayerData
{
    public List<KeyValuePair<string , string>> MyPlayerDataList = new List<KeyValuePair<string, string>>();
}