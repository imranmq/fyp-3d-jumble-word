﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundAudio : MonoBehaviour {

    public AudioClip Slow;
    public AudioClip Medium;
    public AudioClip Fast;
    private AudioSource source;
    private string selectedDifficultyToPlayMusic = "Medium";
    private bool setToEasy = false;
    private bool setToMed = false;
    private bool setToHard = false;
    // Use this for initialization
    void Awake () {
        //GameObject MainGameManagerObject = GameObject.Find("GameManager");
        //selectedDifficultyToPlayMusic =  MainGameManagerObject.GetComponent<BoardManager>().getDifficulty();
        //Debug.Log("DIfficulty : " + selectedDifficultyToPlayMusic);
        source = gameObject.GetComponent<AudioSource>();

    }

    public void TurnOFFMusic() {
 
            source.Pause();
 
    }

    public void TurnOnMusic()
    {              
            source.Play();

    }

	void Update () {
        if (GameManager.difficulty == "Easy" && setToEasy == false)
        {          
         source.clip = Slow;
            setToEasy = true;
            source.Play();
        }
        if (GameManager.difficulty == "Medium" && setToMed == false)
        {
            source.clip = Medium;
            setToMed = true;
            source.Play();
        }
        if (GameManager.difficulty == "Hard" && setToHard == false)
        {
            source.clip = Fast;
            setToHard = true;
            source.Play();
        }
    }
}
