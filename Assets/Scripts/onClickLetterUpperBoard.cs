﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class onClickLetterUpperBoard : MonoBehaviour {
       
    public void OnMouseDown(){
     
        string letter = "";
		if(this.gameObject){       
            //Debug.Log(" HAS CLICK : " + gameObject.GetComponent<TextMesh>().text);
              letter =  gameObject.GetComponent<TextMesh>().text;
			  gameObject.SetActive(false);
			  GameObject SelectedWordObject = GameObject.Find("SelectedLetters");
			  GameObject Manager = GameObject.Find("GameManager");
			  SelectedWordObject.GetComponent<SelectedLetters>().setBoardString(letter, true);
			  Manager.GetComponent<BoardManager>().selectedLettersList(gameObject);
		}
	}
	
}
