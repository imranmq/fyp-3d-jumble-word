﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ToggleMusicOnOFF : MonoBehaviour {
    private GameObject AudioBox;
    private void Start()
    {
        AudioBox = GameObject.Find("AudioBox");
    }

    public void onChangeToggle()
    {
        
        bool ToggleValue = gameObject.GetComponent<Toggle>().isOn;
        if (ToggleValue)
        {
            AudioBox.GetComponent<BackGroundAudio>().TurnOnMusic();
        }
        else
        {
            AudioBox.GetComponent<BackGroundAudio>().TurnOFFMusic();
        }
        Debug.Log(ToggleValue);
    }
}
